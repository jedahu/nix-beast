{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  boot = {
    cleanTmpDir = true;
    initrd.supportedFilesystems = ["ext4"];

    loader.grub = {
      enable = true;
      device = "/dev/sda";
      version = 2;
    };
  };

  environment = {
    systemPackages = with pkgs; [
      acpi
      ag
      alock
      alsaTools
      aumix
      bar
      bspwm
      buku
      curl
      dmenu2
      dos2unix
      elinks
      emacs
      ffmpeg
      file
      flow
      freerdp
      gimp
      gitFull
      gitAndTools.hub
      gitAndTools.tig
      gitAndTools.topGit
      glibc
      gnumake
      gnupg
      google-chrome
      handbrake
      imagemagick
      iotop
      jhead
      libelf
      libreoffice
      lynx
      maim
      mpc_cli
      mpv
      ncdu
      ncmpcpp
      nix-prefetch-git
      nodejs
      obconf
      pam_usb
      pandoc
      pass
      patchelf
      pavucontrol
      pinentry
      rofi
      rofi-pass
      rxvt_unicode
      sbt
      scala
      skype
      sl
      source-code-pro
      sxhkd
      travis
      tmux
      tree
      unrar
      unzip
      vim
      vlc
      wget
      which
      xclip
      xorg.xcursorthemes
      xorg.xev
      xorg.xmodmap
      xsel
      youtube-dl
      zip
    ];

    variables = {
      "SSL_CERT_FILE" = "/etc/ssl/certs/ca-bundle.crt";
    };
  };

  fonts = {
    enableCoreFonts = true;
    enableFontDir = true;
    fonts = with pkgs; [
      corefonts
      inconsolata
      source-code-pro
      ubuntu_font_family
    ];
  };

  hardware = {
    bluetooth.enable = true;
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
      tcp = {
        enable = true;
        anonymousClients = {
          allowAll = false;
          allowedIpRanges = ["127.0.0.1"];
        };
      };
    };
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_NZ.UTF-8";
  };

  networking = {
    extraHosts = ''
      147.75.92.71 colossus
    '';
    hostName = "beast";
    nameservers = [ "8.8.8.8" "8.8.4.4" ];
    wireless.enable = true;
  };

  nix = {
    binaryCachePublicKeys = [
      "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
    ];
    trustedBinaryCaches = [
      "https://nixcache.reflex-frp.org"
    ];
    extraOptions = "auto-optimise-store = true";
    nixPath = ["/etc/nixos" "nixos-config=/etc/nixos/configuration.nix"];
  };

  nixpkgs.config = {
    allowUnfree = true;
    chromium.enableWideVine = true;
    packageOverrides = pkgs: {
      bluez = pkgs.bluez5;
    };
  };

  programs = {
    bash.enableCompletion = true;
    ssh.startAgent = false;
  };

  security = {
    pam = {
      enableSSHAgentAuth = true;
      # services = [
      #   {
      #     name = "usb-auth";
      #     usbAuth = true;
      #   }
      # ];
      # usb.enable = true;
      services.sudo.sshAgentAuth = true;
    };
    sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };
  };

  services = {
    devmon.enable = true;
    mopidy = {
      enable = true;
      extensionPackages = [
        pkgs.mopidy-mopify
        pkgs.mopidy-gmusic
        # pkgs.mopidy-spotify
        # pkgs.mopidy-spotify-tunigo
      ];
      extraConfigFiles = [
        "/etc/mopidy-extra.conf"
      ];
    };
    openssh = {
      enable = true;
      challengeResponseAuthentication = false;
      passwordAuthentication = false;
      startWhenNeeded = true;
      permitRootLogin = "no";
    };
    udisks2.enable = true; # required by pam_usb
    xserver = {
      enable = true;
      displayManager.lightdm.enable = true;
      layout = "us";
      synaptics.enable = true;
      windowManager.openbox.enable = true;
      xkbOptions = "eurosign:e";
    };
  };

  system.stateVersion = "17.03";

  time.timeZone = "Pacific/Auckland";

  users.extraUsers.jdh = {
    extraGroups = ["networkmanager" "wheel"];
    isNormalUser = true;
    uid = 1001;
  };

  virtualisation = {
    docker.enable = true;
  };
}
